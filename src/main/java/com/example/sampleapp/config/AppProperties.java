package com.example.sampleapp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * Properties are configured in the application.yml file.
 * </p>
 */
@ConfigurationProperties(prefix = "app", ignoreUnknownFields = false)
@Getter
@SuppressWarnings({"FieldMayBeFinal", "WeakerAccess"})
public class AppProperties {

    private final Async async = new Async();

    private final Http http = new Http();

    private final Cache cache = new Cache();

    private final Mail mail = new Mail();

    private final Security security = new Security();

    private final Swagger swagger = new Swagger();

    private final Metrics metrics = new Metrics();

    private final CorsConfiguration cors = new CorsConfiguration();

    private final Ribbon ribbon = new Ribbon();
    private final Logging logging = new Logging();

    @Getter
    @Setter
    public static class Async {

        private int corePoolSize = 2;

        private int maxPoolSize = 50;

        private int queueCapacity = 10000;
    }

    @Getter
    public static class Http {

        private final Cache cache = new Cache();

        @Getter
        @Setter
        public static class Cache {

            private int timeToLiveInDays = 1461;
        }
    }

    @Getter
    @Setter
    public static class Cache {

        private int timeToLiveSeconds = 3600;
    }

    @Getter
    @Setter
    public static class Mail {

        private String from = "SampleProject@localhost";
    }

    @Getter
    public static class Security {

        private final Authentication authentication = new Authentication();

        @Getter
        public static class Authentication {

            private final Jwt jwt = new Jwt();

            @Getter
            @Setter
            public static class Jwt {

                private String secret;

                private long tokenValidityInSeconds = 1800;
                private long tokenValidityInSecondsForRememberMe = 2592000;
            }
        }
    }

    @Getter
    @Setter
    public static class Swagger {

        private String title = "SampleProject API";

        private String description = "SampleProject API documentation";

        private String version = "0.0.1";

        private String termsOfServiceUrl;

        private String contactName;

        private String contactUrl;

        private String contactEmail;

        private String license;

        private String licenseUrl;
    }

    @Getter
    public static class Metrics {

        private final Jmx jmx = new Jmx();

        private final Spark spark = new Spark();

        private final Graphite graphite = new Graphite();

        private final Logs logs = new Logs();

        @Getter
        @Setter
        public static class Jmx {

            private boolean enabled = true;
        }

        @Getter
        @Setter
        public static class Spark {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 9999;
        }

        @Getter
        @Setter
        public static class Graphite {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 2003;

            private String prefix = "SampleProject";
        }

        @Getter
        @Setter
        public static class Logs {

            private boolean enabled = false;

            private long reportFrequency = 60;
        }
    }

    @Getter
    public static class Logging {

        private final Logstash logstash = new Logstash();

        @Getter
        @Setter
        public static class Logstash {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 5000;

            private int queueSize = 512;
        }
    }

    @Setter
    @Getter
    public static class Ribbon {

        private String[] displayOnActiveProfiles;

        public String[] getDisplayOnActiveProfiles() {
            return displayOnActiveProfiles;
        }

        public void setDisplayOnActiveProfiles(String[] displayOnActiveProfiles) {
            this.displayOnActiveProfiles = displayOnActiveProfiles;
        }
    }
}
