package com.example.sampleapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sampleapp.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {

}
