(function() {
    'use strict';

    angular
        .module('sampleProjectApp')
        .factory('AppMetricsService', AppMetricsService);

    AppMetricsService.$inject = ['$rootScope', '$http'];

    function AppMetricsService ($rootScope, $http) {
        var service = {
            getMetrics: getMetrics,
            threadDump: threadDump
        };

        return service;

        function getMetrics () {
            return $http.get('management/app/metrics').then(function (response) {
                return response.data;
            });
        }

        function threadDump () {
            return $http.get('management/dump').then(function (response) {
                return response.data;
            });
        }
    }
})();
