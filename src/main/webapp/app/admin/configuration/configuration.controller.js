(function() {
    'use strict';

    angular
        .module('sampleProjectApp')
        .controller('AppConfigurationController', AppConfigurationController);

    AppConfigurationController.$inject = ['$filter','AppConfigurationService'];

    function AppConfigurationController (filter,AppConfigurationService) {
        var vm = this;

        vm.allConfiguration = null;
        vm.configuration = null;

        AppConfigurationService.get().then(function(configuration) {
            vm.configuration = configuration;
        });
        AppConfigurationService.getEnv().then(function (configuration) {
            vm.allConfiguration = configuration;
        });
    }
})();
